'use strict';

var
  webpack = require('webpack'),
  BowerWebpackPlugin = require('bower-webpack-plugin'),
  path = require('path'),
  current = process.cwd(),
  viewDir = 'www';

module.exports = {
  dest: {
    sprite: {
      img: 'client/assets/images',
      css: 'client/common/'
    },
    dev: {
      root: '.tmp'
    },
    build: {
      root: 'build',
      server: 'build/server',
      client: 'build/public',
      assets: 'build/public/assets'
    }
  },

  favicon: {
    src: 'client/favicon.ico'
  },
  assets: {
    src: ['client/assets/**/*', '!client/assets/sprite', '!client/assets/sprite/**/*'],
  },

  sprite: {
    src: ['client/assets/sprite/*.png'],
    option: {
      imgName: '_sprite.png',
      cssName: '_sprite.scss',
      imgPath: '/assets/images/_sprite.png',
      cssFormat: 'scss',
      cssVarMap: function (sprite) {
        sprite.name = 'sprite-' + sprite.name;
      }
    }
  },

  sass: {
    src: ['client/'+viewDir+'/**/*.scss'],
    rename: function (path) {
      path.dirname = 'css';
    }
  },

  ejs: {
    src: ['client/'+viewDir+'/**/*.ejs', '!client/'+viewDir+'/**/_*'],
    viewDir: viewDir,
    rename: function (path) {
      if (path.basename !== 'index') {
        path.basename = 'index';
      }
    }
  },

  js: {
    entry: ['client/'+viewDir+'/**/*.js'],
    src: ['client/components/**/*', 'client/lib/**/*'],
    rename: function (path) {
      path.dirname = 'js';
    }
  },
  webpack: {
    entry: {},
    output: {
      filename: '[name].js',
      sourceMapFilename: '[name].map'
    },
    resolve: {
      root: [
        path.join(current, '/client/' + viewDir),
        path.join(current, '/client/lib'),
        path.join(current, '/client/components'),
        path.join(current, '/client/bower_components')
      ],
      extensions: ['', '.js']
    },
    plugins: [
      new BowerWebpackPlugin(),
      new webpack.ProvidePlugin({
        jQuery: "jquery",
        'window.jQuery': 'jquery',
        $: "jquery",
        _: 'lodash'
      })
    ]
  },
  webpackOption: {
    dev: {
      cache: true,
      debug: true,
      devtool: '#source-map'
    },
    build: {
      cache: false,
      debug: false,
      devtool: ''
    }
  },

  watch: {
    ejs: ['client/common/**/*.ejs', 'client/'+viewDir+'/**/*.ejs', 'client/components/**/*.ejs'],
    sass: ['client/common/**/*.scss', 'client/'+viewDir+'/**/*.scss', 'client/components/**/*.scss'],
    js: ['client/'+viewDir+'/**/*.js', 'client/components/**/*.js', 'client/lib/**/*.js']
  },

  browser: {
    proxy: "http://localhost:9012",
    port: 7000,
    notify: false
  },

  nodeman: {
    script: 'server/app.js',
    ext: 'js ejs',
    ignore: ['./client', 'node_modules'],
    execMap: {
      //js: "node --harmony --use_strict", //doesn't work with socket.io
      js: "node --harmony"
      //js: "babel-node"
    },
    env: {
      NODE_ENV: 'development',
      PORT: 9012,

      DOMAIN: 'http://localhost:9012',
      SESSION_SECRET: 'fullstack-secret',

      FACEBOOK_ID: 'app-id',
      FACEBOOK_SECRET: 'secret',

      TWITTER_ID: 'app-id',
      TWITTER_SECRET: 'secret',

      GOOGLE_ID: 'app-id',
      GOOGLE_SECRET: 'secret',
    }
  },

  conf: {
    src: ['package.json', 'process.json']
  },
  server: {
    src: ['server/**/*']
  }
};
