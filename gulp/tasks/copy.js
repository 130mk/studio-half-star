var
  gulp = require('gulp'),
  conf = require('../config');

gulp.task('copy', function () {
  gulp.src(conf.conf.src)
    .pipe(gulp.dest(conf.dest.build.root));

  gulp.src(conf.server.src)
    .pipe(gulp.dest(conf.dest.build.server));

  gulp.src(conf.favicon.src)
    .pipe(gulp.dest(conf.dest.build.client));

});
