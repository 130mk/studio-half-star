var
  gulp = require('gulp'),
  gulpif = require('gulp-if'),
  imagemin = require('gulp-imagemin'),
  conf = require('../config');

gulp.task('b.assets', function () {
  gulp.src(conf.assets.src)
    .pipe(gulpif('*.{png,jpg,gif}', imagemin()))
    .pipe(gulp.dest(conf.dest.build.assets));
});
