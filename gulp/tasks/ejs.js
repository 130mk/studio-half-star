var
    gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    ejs = require('gulp-ejs'),
    replace = require('gulp-replace'),
    rename = require('gulp-rename'),
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    uglify = require("gulp-uglify"),
    minifyHtml = require('gulp-minify-html'),
    browser = require('browser-sync'),
    conf = require('../config');

gulp.task('ejs', function () {
    return gulp.src(conf.ejs.src)
        .pipe(plumber())
        .pipe(ejs(conf.ejs, {"ext": '.html'}))
        .pipe(replace(/(\.\.\/)+/g, '/'))
        .pipe(rename(conf.ejs.rename))
        .pipe(gulp.dest(conf.dest.dev.root))
        .pipe(browser.reload({stream:true}));
});

gulp.task('b.ejs', function () {
    //var assets = useref.assets();

    gulp.src(conf.ejs.src)
        .pipe(ejs(conf.ejs, {"ext": '.html'}))

        //.pipe(assets)
        //.pipe(gulpif('*.js', uglify()))
        //.pipe(assets.restore())
        .pipe(useref())
        .pipe(gulpif('*.html', replace(/(\.\.\/)+/g, '/')))
        .pipe(gulpif('*.html', minifyHtml({conditionals: true})))
        .pipe(gulpif('*.html', rename(conf.ejs.rename)))
        .pipe(gulp.dest(conf.dest.build.client))
});