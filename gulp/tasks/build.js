var
  gulp = require('gulp'),
  runSequence = require('run-sequence');

gulp.task('build', function (cb) {
  return runSequence(
    'b.clean',
    ['b.assets', 'b.sass', 'b.ejs', 'b.webpack'],
    'copy',
    cb
  );
});