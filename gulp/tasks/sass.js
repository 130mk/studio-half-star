var
  gulp = require('gulp'),
  plumber = require("gulp-plumber"),
  rename = require('gulp-rename'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  autoprefixer = require("gulp-autoprefixer"),
  minifyCss = require("gulp-minify-css"),
  browser = require('browser-sync'),
  conf = require('../config');

gulp.task('sass', function () {
  return gulp.src(conf.sass.src)
      .pipe(plumber({
        errorHandler: function(err) {
          console.log(err.messageFormatted);
          this.emit('end');
        }
      }))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('.'))
    .pipe(rename(conf.sass.rename))
    .pipe(gulp.dest(conf.dest.dev.root))
    .pipe(browser.reload({stream:true}));
});

gulp.task('b.sass', function () {
  gulp.src(conf.sass.src)
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(minifyCss())
    .pipe(rename(conf.sass.rename))
    .pipe(gulp.dest(conf.dest.build.client))
});