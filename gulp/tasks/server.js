var
  gulp = require('gulp'),
  nodemon = require('gulp-nodemon'),
  browser = require('browser-sync'),
  conf = require('../config');


gulp.task('server', ['nodemon'], function() {
  setTimeout(function () {
    browser.init(null, conf.browser);
  }, 1000);
});

gulp.task('nodemon', function (cb) {
  var started = false;
  return nodemon(conf.nodeman)
    .on('start', function () {
      if (!started) {
        cb();
        started = true;
      }
    })
    .on('restart', function () {
      setTimeout(function () {
        browser.reload();
      }, 2000);
    });
});