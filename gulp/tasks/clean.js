var
  gulp = require('gulp'),
  del = require('del'),
  conf = require('../config');

gulp.task('clean', del.bind(null, [conf.dest.dev.root]));
gulp.task('b.clean', del.bind(null, [conf.dest.build.root]));