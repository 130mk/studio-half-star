var
  _ = require('lodash'),
  gulp = require('gulp'),
  plumber = require("gulp-plumber"),
  rename = require('gulp-rename'),
  webpack = require('gulp-webpack'),
  uglify = require("gulp-uglify"),
  browser = require('browser-sync'),
  conf = require('../config');

gulp.task('webpack.entry', function () {
  return gulp.src(conf.js.entry)
    .pipe(rename(function (path) {
      if (path.dirname === '.') {
        conf.webpack.entry[path.basename] = path.basename;
      } else {
        (function (name) {
          conf.webpack.entry[name] = name;
        })(path.dirname + '/' + path.basename);
      }
    }))
});

gulp.task('webpack', ['webpack.entry'], function () {
  var c = _.merge({}, conf.webpack, conf.webpackOption.dev);

  return gulp.src(conf.js.src)
    .pipe(plumber())
    .pipe(webpack(c))
    .pipe(rename(conf.js.rename))
    .pipe(gulp.dest(conf.dest.dev.root))
    .pipe(browser.reload({stream:true}))
});

gulp.task('b.webpack', ['webpack.entry'], function () {
  var c = _.merge({}, conf.webpack, conf.webpackOption.build);

  gulp.src(conf.js.src)
    .pipe(webpack(c))
    .pipe(uglify())
    .pipe(rename(conf.js.rename))
    .pipe(gulp.dest(conf.dest.build.client))
});