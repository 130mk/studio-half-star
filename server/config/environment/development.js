'use strict';

// Development specific configuration
// ==================================
module.exports = {
    mail: 'ikeda@framelunch.jp',
    bcc: {
        innetwork: 'trapdoor6@skimskitta.com'
    }
  //mail: 'k.yamamoto@framelunch.jp'
};
