/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var
  express = require('express'),
  config = require('./config/environment'),

  app = express(),
  server = require('http').createServer(app),
  socketio = require('socket.io')(server, {
    serveClient: true,
    path: '/socket.io-client'
  });

require('./config/socketio')(socketio);
require('./config/express')(app);
require('./routes')(app);
//require('./cron')();

// Start server
server.listen(config.port, config.ip, function () {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
  console.log('mailaddress->' + config.mail + '\n');
});

// Expose app
exports = module.exports = app;