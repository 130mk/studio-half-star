'use strict';

var
  redis = require('../components/redis').client,
  CronJob = require('cron').CronJob,
  generate = function (time, tick, mess) {
    var job = new CronJob(
      time,
      function () {
        tick();
        redis.lpush('log:cron', mess + ':' + (new Date()).toLocaleString());
        redis.ltrim('log:cron', 0, 99);
      },
      function () {
        console.log('job complete');
      },
        false,
        'Asia/Tokyo'
    );

    job.start();
    return job;
  };

module.exports = function () {
  generate('00 00 00 * * *', require('../api/user/user.status').update, 'update user status');
  //generate('*/5 * * * * *', require('../api/user/user.status').update, 'update user status');
};
