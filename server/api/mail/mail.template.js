'use strict';

var env = require('../../config/environment');

module.exports.reservation = function (body) {
    var
        obj = {},
        str = '';

    obj.from = 'info@studiohalfstar.com';
    obj.to = body.email;
    obj.bcc = require('../../config/environment').mail;
    obj.subject = '【STUDIO HALF STAR】' + '予約申し込み完了';

    str += body.name + '様\n\n';
    str += 'STUDIO HALF STARへ予約・お問合せ頂きありがとうございます。\n';
    str += '以下の情報で承りました。\n\n';
    str += 'お名前 : ' + body.name + '\n';
    str += '電話番号 : ' + body.tel + '\n';
    str += 'メールアドレス : ' + body.email + '\n';
    str += '予約・お問い合わせ内容 : \n' + body.contact + '\n\n';

    str += 'スタジオ担当者より追ってご連絡いたします。\n';

    obj.text = str;
    return obj;
};

module.exports.event = function (body) {
    var
        obj = {},
        str = '';

    obj.from = 'info@studiohalfstar.com';
    obj.to = body.email;
    obj.bcc = env.mail + ',' + env.bcc[body.bcc];
    obj.subject = '【STUDIO HALF STAR】' + body.title + 'お申しこみ完了';

    str += body.name + '様\n\n';
    str += 'この度は「' + body.title + '」へお申し込み頂きありがとうございます。\n';
    str += '以下の情報で承りました。\n\n';
    str += 'お名前 : ' + body.name + '\n';
    str += '電話番号 : ' + body.tel + '\n';
    str += 'メールアドレス : ' + body.email + '\n';
    str += '参加希望日時 - 部・押しのタレント : \n' + body.contact + '\n\n';

    //str += 'スタジオ担当者より追ってご連絡いたします。\n';

    obj.text = str;
    return obj;
};
