'use strict';

var
  express = require('express'),
  router = express.Router(),
  template = require('./mail.template'),
  nodemailer = require('nodemailer'),
  transport = nodemailer.createTransport({
    service: "Zoho",
    auth: {
      user: "info@studiohalfstar.com",
      pass: "42s805h10s2"
    }
  }),
  send = function (template) {
    return function (req, res) {
      transport.sendMail(template(req.body), function(err, mail){
        transport.close();
        return res.json(200, {});
      });
    }
  };

router.post('/reservation', send(template.reservation));
router.post('/event', send(template.event));

module.exports = router;
