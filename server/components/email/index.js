/**
 * Created by pond6 on 10/28/14.
 */
'use strict';

var
  email = require('mailer'),
  sendMail;

sendMail = function (obj, cb) {
  email.send(
    {
      host: 'smtp.sendgrid.net',
      port: '2525',
      domain: 'smtp.sendgrid.net',
      authentication: 'login',
      username: 'framelunch',
      password: 'framelunch1980',
      to: obj.to,
      from: obj.from || 'noreply@ssuc.co.jp',
      subject: obj.subject,
      body: obj.message
    },
    function (err, result) {
      cb(err, result);
    }
  );
};

module.exports = {
  send : sendMail
};
