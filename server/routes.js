/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');

module.exports = function (app) {
  // Insert routes below
  app.use('/api/mail', require('./api/mail'));

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function (req, res) {
      res.writeHead(301, { //You can use 301, 302 or whatever status code
        'Location': '/',
        'Expires': (new Date()).toGMTString()
      });
      res.end();
    });
};
