'use strict';

require('button/btMenu');
require('button/btSns');

var
  scroll = require('scroll'),
  events = require('events'),
  header = require('header/header'),
  header_sp = require('header_sp/header_sp'),
  form = require('form')
  ;

scroll.init();
events.init();

header.init();
header_sp.init();

form.init();
$('html body').css('opacity', 1);