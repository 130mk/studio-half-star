'use strict';

require('button/btMenu');
require('button/btSns');
require('button/btStar');
require('button/btThumb');

var
  kvs = require('kvs'),
  scroll = require('scroll'),
  events = require('events'),
  slider = require('slider'),
  loopImage = require('loopImage'),
  header = require('header/header'),
  header_sp = require('header_sp/header_sp'),
  popup = require('popup/popup'),
  gototop = require('gototop/gototop')
  ;

scroll.init();
events.init();

header.init();
header_sp.init();
gototop.init();

slider.init();
slider.start();

loopImage.init();
loopImage.start();

popup.init({
  opened: function () {
    slider.stop();
    loopImage.stop();
  },
  closed: function () {
    slider.start();
    loopImage.start();
  }
});
kvs.set('popup', popup);

$('html body').css('opacity', 1);