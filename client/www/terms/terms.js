'use strict';

require('button/btMenu');
require('button/btSns');

var
  scroll = require('scroll'),
  events = require('events'),
  header = require('header/header'),
  header_sp = require('header_sp/header_sp'),
  gototop = require('gototop/gototop')
  ;

scroll.init();
events.init();

header.init();
header_sp.init();
gototop.init();