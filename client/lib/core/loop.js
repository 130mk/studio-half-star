'use strict';
var
  cp = require('./Cp')(),
  time = 40,
  id;

module.exports = {
  add: function (func) {
    cp.add(func);
    if (!id) {
      id = setInterval(function(){ cp.publish(); }, time);
    }
    return func;
  },
  remove: function (func) {
    cp.remove(func);
    if (!cp.length) {
      clearInterval(id);
      id = null;
    }
  }
};