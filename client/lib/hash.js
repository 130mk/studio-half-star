'use strict';

var state = require('./core/State')();

module.exports = {
  init: function () {
    if ($) {
      $(window)
        .bind('hashchange', function() {
          var s = location.hash.replace('#','');
          state.change(s);
        })
        .trigger('hashchange');
      return true;
    } else {
      return false;
    }
  },
  change: function (v) {
    location.hash = v;
  },
  listen: function (key, func) {
    return state.listen(key, func);
  },
  clear: function (key, func) {
    state.clear(key, func);
  },

  wait: function () { state.wait(); },
  notify: function () { state.notify(); }
};