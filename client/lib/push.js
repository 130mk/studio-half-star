'use strict';

var state = require('./core/State')();

module.exports = {
  init: function () {
    if ($ && window.history && window.history.pushState) {
      $(window)
        .on('popstate', function() {
          /** not yet **/
          var s = history.state || '';
          state.change(s);
        })
        .trigger('popstate');
      return true;
    } else {
      return false;
    }
  },
  change: function (v) {
    window.history.pushState(v, null, v);
    state.change(v);
  },
  listen: function (key, func) {
    return state.listen(key, func);
  },
  clear: function (key, func) {
    state.clear(key, func);
  },

  wait: function () { state.wait(); },
  notify: function () { state.notify(); }
};