'use strict';

var
  loop = require('core/loop'),
  $view = $('.loopImage'),
  images = [],

  init = function () {
    $view.css({
      position: 'relative',
      overflow: 'hidden'
    });

    $view.each(function () {
      (function ($this, arr, $li) {
        $li = $this.find('li');
        $li.css({
          position:'absolute'
        });

        $li.each(function (index) {
          (function ($this) {
            $this.offset({left:$this.width()*index});
            arr.push($this);
          })(
            $(this)
          );
        });
        images.push(arr);
      })(
        $(this),
        []
      );
    });
  },

  move = function () {
    $view.each(function (index, arr) {
      arr = images[index];

      (function ($li) {
        if ($li.offset().left < -$li.width()) {
          arr.shift();
          arr.push($li);
          $li = arr[0];
        }
        $li.offset({left: $li.offset().left - 1});
      })(
        arr[0]
      );

      _.forEach(arr, function ($li, i) {
        if (i===0) { return; }

        (function ($pre) {
          $li.offset({left: $pre.offset().left + $pre.width()});
        })(
          arr[i-1]
        );

      });
    });

  },
  start = function () {
    loop.add(move);
  },
  stop = function () {
    loop.remove(move);
  };

module.exports = {
  init: init,
  start: start,
  stop: stop
};