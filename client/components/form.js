'use strict';

var
    $input = $('#input'),
    $name = $('#name'),
    $tel = $('#tel'),
    $email = $('#email'),
    $contact = $('#contact'),
    $terms = $('#terms'),
    $errName = $('#err-name'),
    $errTel = $('#err-tel'),
    $errEmail = $('#err-email'),
    $errContact = $('#err-contact'),
    $errTerms = $('#err-terms'),
    $btConfirm = $('#bt-confirm'),

    $confirm = $('#confirm'),
    $cfName = $('#confirm-name'),
    $cfTel = $('#confirm-tel'),
    $cfEmail = $('#confirm-email'),
    $cfContact = $('#confirm-contact'),
    $btReturn = $('#bt-return'),
    $btSend = $('#bt-send'),

    $complete = $('#complete'),
    $btTop = $('#bt-top'),

    type,
    title,
    bcc,

    init = function () {
        type = $input.attr("_type") || 'reservation';
        title = $input.attr("_title") || '';
        bcc = $input.attr("_bcc") || '';

        $('em').css({display: 'none'});
        $confirm.css({display: 'none'});
        $complete.css({display: 'none'});

        $btConfirm.on('click', onConfirm);
        $btReturn.on('click', onReturn);
        $btSend.on('click', onSend);
        $btTop.on('click', onTop);

        $('html body').css('opacity', 1);
    },
    validation = function (val, err, func) {
        func = func || function () { return true; };

        if (!val || !func(val)) {
            err.css({display: ''});
            return false;
        } else {
            err.css({display: 'none'});
            return true;
        }
    },
    onConfirm = function (e) {
        e.preventDefault();

        (function (bool) {
            if (!validation($name.val(), $errName, null)) {
                bool = false;
            }
            if (!validation($tel.val(), $errTel, function (val) {
                    return val.match(/^(0[0-9]{1,4}[-]?)?[0-9]{1,4}[-]?[0-9]{4}$/);
                })
            ) {
                bool = false;
            }
            if (!validation($email.val(), $errEmail, function (val) {
                    return val.match(/^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]{1,}\.[A-Za-z0-9]{1,}$/);
                })
            ) {
                bool = false;
            }

            if (!validation($contact.val(), $errContact, null)) {
                bool = false;
            }

            if (type !== 'event') {
                if (!validation($terms.prop('checked'), $errTerms, null)) {
                    bool = false;
                }
            }

            if (bool) {
                $cfName.text($name.val());
                $cfTel.text($tel.val());
                $cfEmail.text($email.val());
                $cfContact.html($contact.val().replace(/\n/g, '<br>'));
                $input.css({display: 'none'});
                $confirm.css({display: ''});

                var $w = $(window);
                $w.scrollTop($confirm.offset().top - 100);
            }
        })(true);
    },
    onReturn = function (e) {
        e.preventDefault();
        $input.css({display: ''});
        $confirm.css({display: 'none'});
    },
    onSend = function (e) {
        e.preventDefault();

        $.post('/api/mail/' + type, {
                title: title,
                bcc: bcc,
                name: $name.val(),
                tel: $tel.val(),
                email: $email.val(),
                contact: $contact.val()
            })
            .done(function (data) {
                //console.log(data);
            });

        $confirm.css({display: 'none'});
        $complete.css({display: ''});
        $(window).scrollTop($complete.offset().top - 100);
    },
    onTop = function (e) {
        location.href = '/';
    };

module.exports = {
    init: init
};
