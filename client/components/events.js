'use strict';

var
  notice = require('notice'),
  init = function () {
    (function ($window, a) {
      a = 0;
      $window.on('scroll', function () {
        (function (top) {
          a = top;
          if (top !== 0) {
            notice.publish('scroll', [top]);
          } else {
            setTimeout(function () {
              if (a === 0) {
                notice.publish('scroll', [a]);
              }
            }, 100);
          }
        })($window.scrollTop());
      });

      $window.on('resize', function () {
        notice.publish('resize', [$window]);
      });

    }(
      $(window)
    ));
  };

module.exports = {
  init: init
};

