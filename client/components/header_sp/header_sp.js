require('velocity');

var
  $open = $('.header-sp__open'),
  $close = $('.header-sp__close'),
  $target = $('.header-sp__menu__target'),
  $menu = $('.header-sp__menu'),
  notice = require('notice'),
  mHeight = $menu.height(),

  init = function () {
    $open.on('click', onOpen);
    $close.on('click', onClose);
    $target.on('click', onTarget);

    notice.listen('scroll', onScroll);
  },
  onOpen = function () {
    $open.hide();
    $close.show();

    $menu.css({height:0, display:'block'});
    $menu.velocity('stop');
    $menu.velocity({height:mHeight}, {duration:300});
  },
  onClose = function () {
    $open.show();
    $close.hide();

    $menu.velocity('stop');
    $menu.velocity({height:0}, {duration:300, complete: function () {
      $menu.css({display: 'none'});
    }});
  },
  onTarget = function () {
    $open.show();
    $close.hide();
    $menu.velocity('stop');
    $menu.hide();
  },
  onScroll = function () {
    $open.show();
    $close.hide();

    $menu.velocity('stop');
    $menu.hide();
  }
  ;

module.exports = {
  init: init
};
