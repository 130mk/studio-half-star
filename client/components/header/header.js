'use strict';

require('velocity');

var
  notice = require('notice'),
  $header = $('.header'),
  $dummy = $("<div class='header__dummy'></div>"),
  isFixed = false,

  init = function () {
    notice.listen('scroll', onScroll);

    $dummy.height($header.innerHeight());
    $header.after($dummy);
    $header.css({
      position: 'fixed',
      top: '0px',
      left: '0px',
      'box-shadow': '0 0 0 #888888',
      'z-index': 88888
    });
  },
  onScroll = function (scrollTop) {
    if (scrollTop < $header.innerHeight()*2) {
      if (!isFixed) {
        $header.css({top: -scrollTop + 'px'});
      }

      if (scrollTop === 0) {
        isFixed = false;
        $header.css({top: '0px'});
        $header.velocity('stop');
        $header.velocity({boxShadowBlur: 0}, {duration: 100});
      }

    } else {
      if (!isFixed) {
        isFixed = true;

        $header.css({top: '-100px'});
        $header.velocity('stop');
        $header.velocity({top: 0, boxShadowBlur: 5}, {duration: 500, easing: 'easeOut'});
      }
    }
  };

module.exports = {
  init: init
};