'use strict';
require('velocity');

require('./Button')({
  selector: '.btMenu',
  over: function ($) {
    $.velocity('stop');
    $.velocity({opacity: 0.3}, {duration: 200});
  },
  out: function ($) {
    $.velocity('stop');
    $.velocity({opacity: 1}, {duration: 200});
  }
});