'use strict';
require('velocity');

require('./Button')({
  selector: '.btStar',
  init: function ($) {
    $.css({
      cursor: 'pointer',
      opacity: 0
    });
  },
  over: function ($) {
    $.velocity('stop');
    $.velocity({opacity: 1}, {duration: 200});
  },
  out: function ($) {
    $.velocity('stop');
    $.velocity({opacity: 0}, {duration: 200});
  },
  click: function ($) {

  }
});
