'use strict';

var
  kvs = require('kvs'),
  dummy = function () {},
  c = function (p) {
    p.init = p.init || dummy;
    p.over = p.over || dummy;
    p.out = p.out || dummy;
    p.click = p.click || dummy;

    (function ($view) {
      $view.on('mouseover', function () {
        p.over($(this));
      });
      $view.on('mouseout', function () {
        p.out($(this));
      });
      $view.on('click', function () {
        (function ($this) {
          p.click($this);
          (kvs.get($this.attr('_click')) || dummy)($this);
        })
        ($(this));
      });
      p.init($view);

    })(
      $(p.selector)
    );
  };

module.exports = function (p) {
  return new c(p);
};