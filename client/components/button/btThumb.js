'use strict';
require('velocity');

require('./Button')({
  selector: '.btThumb',
  init: function ($) {
    $.css('cursor', 'pointer');
  },
  over: function ($) {
    $.velocity('stop');
    $.velocity({opacity: 0.5}, {duration: 200});
  },
  out: function ($) {
    $.velocity('stop');
    $.velocity({opacity: 1}, {duration: 200});
  },
  click: function ($) {

  }
});