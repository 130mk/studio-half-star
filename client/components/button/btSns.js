'use strict';
require('velocity');

require('./Button')({
  selector: '.btSns',
  init: function ($) {
    $.css('opacity', 0.5);
  },
  over: function ($) {
    $.velocity('stop');
    $.velocity({opacity: 1}, {duration: 200});
  },
  out: function ($) {
    $.velocity('stop');
    $.velocity({opacity: 0.5}, {duration: 200});
  }
});