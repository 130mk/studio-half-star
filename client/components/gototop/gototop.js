'use strict';

require('velocity');

var
  notice = require('notice'),
  $view = $('#gototop'),
  $limit = $('#gototop-limit'),
  $header = $('header'),
  $window = $(window),
  isView = false,

  init = function () {
    $view.css({
      position: 'fixed',
      right: '30px',
      bottom: '20px',
      opacity: 0,
      display: 'none'
    });

    notice.listen('scroll', onScroll);
  },
  onScroll = function (scrollTop) {
    if (scrollTop > $header.innerHeight()) {
      if (!isView) {
        $view.css('display', '');
        $view.velocity('stop');
        $view.velocity({opacity: 1}, {duration:300, delay: 100});
        isView = true;
      }

      ((function (offset, wh) {
        if (offset < wh) {
          $view.css({bottom: wh-offset+20 + 'px'});
        } else {
          $view.css({bottom: '20px'});
        }
      })(
        $limit.offset().top - scrollTop,
        $window.height()
      ))

    } else {
      if (isView) {
        $view.velocity('stop');
        $view.velocity({opacity: 0}, {duration:300, delay: 100, complete: function () {
          $view.css('display', 'none');
        }});
        isView = false;
      }
    }
  };

module.exports = {
  init: init
};
