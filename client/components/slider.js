'use strict';

require('velocity');

var
  notice = require('notice'),
  $mvList = $('.slider'),
  selects = [],
  intervalId,

  init = function () {
    ((function ($li, $img) {
      $mvList.css('position', 'relative');
      $mvList.css({
        position: 'relative',
        width: '100%',
        overflow: 'hidden'
      })
      $li.css('position', 'absolute');
      $img.css('width', '100%');

      $mvList.each(function (index) {
        (function ($this, $dotContainer, $dot) {
          selects[index] = 0;
          $this.find('li').each(function (i) {
            i === selects[index] ?
              $(this).css('opacity', 1) :
              $(this).css({opacity: 0, display: 'none'});

            $dot = $('<span index="'+index+'"/>');
            $dot.css(
              {
                display: 'inline-block',
                width: 8,
                height: 8,
                'border-radius': '50%',
                background: i === selects[index] ? '#4888C8' : '#e5e5d5',
                margin: '20px 7px 0',
                cursor: 'pointer'
              }
            );
            $dot.on('click', click);
            $dotContainer.append($dot);
          });

          $this.append($dotContainer);
        })(
          $(this),
          $('<div></div>')
        );
      });

      notice.listen('resize', resize);
      resize();
    })(
      $mvList.find('li'),
      $mvList.find('img')
    ));
  },

  auto = function () {
    $mvList.each(function (index) {
      selects[index] = (++selects[index]) % $(this).find('li').length;
      change();
    });
  },
  click = function () {
    (function ($this) {
      selects[$this.attr('index')] = $this.index();
      stop();
      change();
      start();
    })($(this));
  },

  change = function () {
    $mvList.each(function (index) {
      ((function ($this, $li, $span, selectedIndex) {
        $li = $this.find('li');
        $span = $this.find('span');
        selectedIndex = selects[index];

        $li.each(function (i) {
          (function ($this) {
            $this.velocity('stop');
            i===selectedIndex ?
              $this.velocity({opacity:1}, {duration:1000, display:''}) :
              $this.velocity({opacity:0}, {duration:1000, display:'none'})

          })($(this));
        });

        $span.each(function (i) {
          i===selectedIndex ?
            $(this).css('background', '#4888C8') :
            $(this).css('background', '#e5e5e5');
        });

      })(
        $(this)
      ));
    });
  },

  resize = function () {
    $mvList.each(function () {
      ((function ($this, $ul, $li, width) {
        $ul = $this.find('.slider__list');
        $li = $this.find('.slider__list li');
        width = $('html body').width();

        if (width < 400) {
          width *=1.5;
        }

        $li.width(width);
        $li.css({left: (width-$li.width())/2});
        //$ul.height($li.height());
        $ul.height(width/2);
      })(
        $(this)
      ));
    });
  },

  start = function () {
    intervalId = setInterval(auto, 5000);
  },

  stop = function () {
    if (intervalId) {
      clearInterval(intervalId);
    }
  };

module.exports = {
  init: init,
  start: start,
  stop: stop
};

