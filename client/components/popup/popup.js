'use strict';

require('velocity');

var
  notice = require('notice'),
  $container = $('#popup'),
  $background = $container.find('#popup-background'),
  $content = $container.find('#popup-content'),
  $img = $container.find('#popup-img'),
  $close = $container.find('#popup-close'),
  $prev = $container.find('#popup-prev'),
  $next = $container.find('#popup-next'),

  selectedIndex = null,
  items = null,
  delegate = null,

  init = function (_delegate) {
    delegate = _delegate;

    $container.css('display', 'none');

    $img.on('load', loadedImage);
    $prev.on('click', prev);
    $next.on('click', next);
    $close.on('click', close);
    $background.on('click', close);
  },

  open = function ($target) {
    if (delegate.opened) {
      delegate.opened();
    }

    notice.listen('resize', resize);
    resize($(window));

    items = (function (arr) {
      var imgs = [];
      $('.' + arr[arr.length-1]).each(function () {
        imgs.push($(this).attr('_src'));
      });
      return imgs;
    })($target.attr('class').split(' '));

    selectedIndex = _.indexOf(items, $target.attr('_src'));

    $img.css('opacity', 0);
    $container.css('display', '');
    $container.velocity('stop');
    $container.velocity({opacity: 1}, {duration:300});
    load();
  },
  load = function () {
    $img.velocity({opacity: 0}, {duration: 200, complete: function () {
      $img.attr('src', items[selectedIndex]);
    }});
  },
  loadedImage = function () {
    $img.css({
      top: Math.floor(($content.height()-$img.height())/2),
      left: Math.floor(($content.width()-$img.width())/2)
    });
    $img.velocity({opacity: 1}, {duration: 300});
  },
  prev = function () {
    --selectedIndex;
    if (selectedIndex<0) {
      selectedIndex = items.length - 1;
    }
    load();
  },
  next = function () {
    ++selectedIndex;
    if (selectedIndex>=items.length) {
      selectedIndex = 0;
    }
    load();
  },
  close = function () {
    notice.clear('resize', resize);

    $container.velocity('stop');
    $container.velocity({opacity: 0}, {duration:300, complete: function () {
      $img.removeAttr('src');
      $container.css('display', 'none');

      if (delegate.closed) {
        delegate.closed();
      }

    }});
  },
  resize = function ($window) {
    $content.css('margin', Math.floor(($window.height()-$content.height())/2)-20 + 'px auto 0');
  };

module.exports = {
  init: init,
  open: open,
  close: close
};
