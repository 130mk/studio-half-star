var
  $a = $('a[href^=#]'),
  speed = 500,

  init = function () {
    $a.click(onClick);
  },
  onClick = function () {
    ((function (target) {
      target.velocity('scroll', {duration: speed, easing: 'swing'});
    })(
      ((function (href) {
        return $(href === "#" || href=="" ? 'html' : href);
      })(
        $(this).attr("href")
      ))
    ));
  };

module.exports = {
  init: init
};